/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    appName: "WriteResume",
  }
}

module.exports = nextConfig
