// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url = env("DATABASE_URL")
}

model User {
  id String @id @default(cuid())
  name String?
  email String? @unique
  emailVerified DateTime?
  image String?
  accounts Account[] //has many social accounts, 1-n
  sessions Session[] //has many sessions, 1-n
  resumes Resume[] //has many resumes, 1-n
  coverLetters CoverLetter[] //has many cover letters, 1-n
}

model Industry {
  id String @id @default(cuid())
  name String
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  archivedAt DateTime?
  professions Profession[] //has many industries, 1-n
  snippets IndustriesOnSnippets[] //has many snippets: many-to-many
}

model Profession {
  id String @id @default(cuid())
  name String
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  archivedAt DateTime?
  industries Industry @relation(fields: [industryId], references: [id]) //belongs to industry
  industryId String
  resumes Resume[] //has many resumes, 1-n
  coverLetters CoverLetter[] //has many cover letters, 1-n
}

model Snippet {
  id String @id @default(cuid())
  type String //eiter of resume, cover-letter
  segment String //for a specific field
  body Json? // text to be shown as a example
  createdAt DateTime   @default(now())
  updatedAt DateTime   @updatedAt
  archivedAt DateTime?
  industries IndustriesOnSnippets[] //can be in many industries: many-to-many
}

model Resume {
  id String @id @default(cuid())
  title String
  body Json? //Objects: info, carrier summary, primary skills, secondary skills, intrests, eduction, internship, experience, achievement, course, language, websites, references
  preference Json? //Objects: Resume category id, Template Id, Show refrences, Show address, Show mobile
  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt
  archivedAt DateTime?
  user User @relation(fields: [userId], references: [id]) //resume belongs to user
  userId String
  profession Profession? @relation(fields: [professionId], references: [id]) //resume belongs to profession
  professionId String?
}

model CoverLetter {
  id String @id @default(cuid())
  title String
  body Json? //Objects: info, body
  preference Json? //Objects: Template Id, other settings
  createdAt DateTime  @default(now())
  updatedAt DateTime  @updatedAt
  archivedAt DateTime?
  user User @relation(fields: [userId], references: [id]) //belongs to user
  userId  String
  profession Profession? @relation(fields: [professionId], references: [id]) //belongs to profession
  professionId String?
}

model IndustriesOnSnippets{
 industry Industry @relation(fields: [industryId], references: [id])
 industryId String //many industries
 snippet Snippet @relation(fields: [snippetId], references: [id])
 snippetId String //many snippets
 createdAt DateTime @default(now())
 @@id([industryId, snippetId])
}

//For oauth logins
model Account {
  id String  @id @default(cuid())
  type String
  provider String
  providerAccountId String
  refresh_token String? @db.Text
  access_token String? @db.Text
  expires_at Int?
  token_type String?
  scope String?
  id_token String? @db.Text
  session_state String?
  user User @relation(fields: [userId], references: [id], onDelete: Cascade) //belong to user
  userId String
  @@unique([provider, providerAccountId])
}

model Session {
  id String   @id @default(cuid())
  sessionToken String   @unique
  expires DateTime
  user User @relation(fields: [userId], references: [id], onDelete: Cascade) //belongs to user
  userId String
}

model VerificationToken {
  identifier String
  token String   @unique
  expires DateTime
  @@unique([identifier, token])
}
