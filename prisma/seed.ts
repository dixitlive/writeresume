import { Prisma, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {

    //insert example user
    await prisma.user.upsert({
        where: { email: 'dixiton@gmail.com' },
        update: {},
        create: {
            email: 'dixiton@gmail.com'
        },
    })

    //create category and professions
    await prisma.industry.create({
        data: {
            name: 'Accounting',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Accounting'},
                        {name: 'Accounts Payable'},
                        {name: 'Accounts Receivable/Credit Control'},
                        {name: 'Analysis & Reporting'},
                        {name: 'Assistant Accountants'},
                        {name: 'Audit - External'},
                        {name: 'Audit - Internal'},
                        {name: 'Bookkeeping & Small Practice Accounting'},
                        {name: 'Business Services & Corporate Advisory'},
                        {name: 'Company Secretaries'},
                        {name: 'Compliance & Risk'},
                        {name: 'Cost Accounting'},
                        {name: 'Financial Accounting & Reporting'},
                        {name: 'Financial Managers & Controllers'},
                        {name: 'Forensic Accounting & Investigation'},
                        {name: 'Insolvency & Corporate Recovery'},
                        {name: 'Inventory & Fixed Assets'},
                        {name: 'Management'},
                        {name: 'Management Accounting & Budgeting'},
                        {name: 'Payroll'},
                        {name: 'Strategy & Planning'},
                        {name: 'Systems Accounting & IT Audit'},
                        {name: 'Taxation'},
                        {name: 'Treasury'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Administration & Office Support',
            professions: {
                createMany: {
                    data:[
                        { name: 'All Administration & Office Support' },
                        { name: 'Administrative Assistants' },
                        { name: 'Client & Sales Administration' },
                        { name: 'Contracts Administration' },
                        { name: 'Data Entry & Word Processing' },
                        { name: 'Office Management' },
                        { name: 'PA, EA & Secretarial' },
                        { name: 'Receptionists1' },
                        { name: 'Records Management & Document Control' },
                    ]
                }
            }
        }
    })

    await prisma.industry.create({
        data: {
            name: 'Advertising, Arts & Media',
            professions: {
                createMany: {
                    data:[
                        { name: 'All Advertising, Arts & Media' },
                        { name: 'Agency Account Management' },
                        { name: 'Art Direction' },
                        { name: 'Editing & Publishing' },
                        { name: 'Event Management' },
                        { name: 'Journalism & Writing' },
                        { name: 'Management' },
                        { name: 'Media Strategy, Planning & Buying' },
                        { name: 'Performing Arts' },
                        { name: 'Photography' },
                        { name: 'Programming & Production' },
                        { name: 'Promotions' },
                    ]
                }
            }
        }
    })

    await prisma.industry.create({
        data: {
            name: 'Banking & Financial Services',
            professions: {
                createMany: {
                    data:[
                        { name: 'All Banking & Financial Services' },
                        { name: 'Account & Relationship Management' },
                        { name: 'Analysis & Reporting' },
                        { name: 'Banking - Business' },
                        { name: 'Banking - Corporate & Institutional' },
                        { name: 'Banking - Retail/Branch' },
                        { name: 'Client Services' },
                        { name: 'Compliance & Risk' },
                        { name: 'Corporate Finance & Investment Banking' },
                        { name: 'Credit' },
                        { name: 'Financial Planning' },
                        { name: 'Funds Management' },
                        { name: 'Management' },
                        { name: 'Mortgages' },
                        { name: 'Settlements' },
                        { name: 'Stockbroking & Trading' },
                        { name: 'Treasury' },
                    ]
                }
            }
        }
    })

    await prisma.industry.create({
        data: {
            name: 'Call Centre & Customer Service',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Call Centre & Customer Service'},
                        {name: 'Collections'},
                        {name: 'Customer Service - Call Centre'},
                        {name: 'Customer Service - Customer Facing'},
                        {name: 'Management & Support'},
                        {name: 'Sales - Inbound'},
                        {name: 'Sales - Outbound'},
                        {name: 'Supervisors/Team Leaders'},
                    ]
                }
            }
        }
    })

    await prisma.industry.create({
        data: {
            name: 'CEO & General Management',
            professions: {
                createMany: {
                    data: [
                        {name: 'All CEO & General Management'},
                        {name: 'Board Appointments'},
                        {name: 'CEO'},
                        {name: 'COO & MD'},
                        {name: 'General/Business Unit Manager'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Community Services & Development',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Community Services & Development'},
                        {name: 'Aged & Disability Support'},
                        {name: 'Child Welfare, Youth & Family Services'},
                        {name: 'Community Development'},
                        {name: 'Employment Services'},
                        {name: 'Fundraising'},
                        {name: 'Housing & Homelessness Services'},
                        {name: 'Indigenous & Multicultural Services'},
                        {name: 'Management'},
                        {name: 'Volunteer Coordination & Support'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Construction',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Construction'},
                        {name: 'Contracts Management'},
                        {name: 'Estimating'},
                        {name: 'Foreperson/Supervisors'},
                        {name: 'Health, Safety & Environment'},
                        {name: 'Management'},
                        {name: 'Planning & Scheduling'},
                        {name: 'Plant & Machinery Operators'},
                        {name: 'Project Management'},
                        {name: 'Quality Assurance & Control'},
                        {name: 'Surveying'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Consulting & Strategy',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Consulting & Strategy'},
                        {name: 'Analysts'},
                        {name: 'Corporate Development'},
                        {name: 'Environment & Sustainability Consulting'},
                        {name: 'Management & Change Consulting'},
                        {name: 'Policy'},
                        {name: 'Strategy & Planning'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Design & Architecture',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Design & Architecture'},
                        {name: 'Architectural Drafting'},
                        {name: 'Architecture'},
                        {name: 'Fashion & Textile Design'},
                        {name: 'Graphic Design'},
                        {name: 'Illustration & Animation'},
                        {name: 'Industrial Design'},
                        {name: 'Interior Design'},
                        {name: 'Landscape Architecture'},
                        {name: 'Urban Design & Planning'},
                        {name: 'Web & Interaction Design'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Education & Training',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Education & Training'},
                        {name: 'Childcare & Outside School Hours Care'},
                        {name: 'Library Services & Information Management'},
                        {name: 'Management - Schools'},
                        {name: 'Management - Universities'},
                        {name: 'Management - Vocational'},
                        {name: 'Research & Fellowships'},
                        {name: 'Student Services'},
                        {name: 'Teaching - Early Childhood'},
                        {name: 'Teaching - Primary'},
                        {name: 'Teaching - Secondary'},
                        {name: 'Teaching - Tertiary'},
                        {name: 'Teaching - Vocational'},
                        {name: 'Teaching Aides & Special Needs'},
                        {name: 'Tutoring'},
                        {name: 'Workplace Training & Assessment'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Engineering',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Engineering'},
                        {name: 'Aerospace Engineering'},
                        {name: 'Automotive Engineering'},
                        {name: 'Building Services Engineering'},
                        {name: 'Chemical Engineering'},
                        {name: 'Civil/Structural Engineering'},
                        {name: 'Electrical/Electronic Engineering'},
                        {name: 'Engineering Drafting'},
                        {name: 'Environmental Engineering'},
                        {name: 'Field Engineering'},
                        {name: 'Industrial Engineering'},
                        {name: 'Maintenance'},
                        {name: 'Management'},
                        {name: 'Materials Handling Engineering'},
                        {name: 'Mechanical Engineering'},
                        {name: 'Process Engineering'},
                        {name: 'Project Engineering'},
                        {name: 'Project Management'},
                        {name: 'Supervisors'},
                        {name: 'Systems Engineering'},
                        {name: 'Water & Waste Engineering'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Farming, Animals & Conservation',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Farming, Animals & Conservation'},
                        {name: 'Agronomy & Farm Services'},
                        {name: 'Conservation, Parks & Wildlife'},
                        {name: 'Farm Labour'},
                        {name: 'Farm Management'},
                        {name: 'Fishing & Aquaculture'},
                        {name: 'Horticulture'},
                        {name: 'Veterinary Services & Animal Welfare'},
                        {name: 'Winery & Viticulture'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Government & Defence',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Government & Defence'},
                        {name: 'Air Force'},
                        {name: 'Army'},
                        {name: 'Emergency Services'},
                        {name: 'Government - Federal'},
                        {name: 'Government - Local'},
                        {name: 'Government - State'},
                        {name: 'Navy'},
                        {name: 'Police & Corrections'},
                        {name: 'Policy, Planning & Regulation'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Healthcare & Medical',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Healthcare & Medical'},
                        {name: 'Ambulance/Paramedics'},
                        {name: 'Chiropractic & Osteopathic'},
                        {name: 'Clinical/Medical Research'},
                        {name: 'Dental'},
                        {name: 'Dieticians'},
                        {name: 'Environmental Services'},
                        {name: 'General Practitioners'},
                        {name: 'Management'},
                        {name: 'Medical Administration'},
                        {name: 'Medical Imaging'},
                        {name: 'Medical Specialists'},
                        {name: 'Natural Therapies & Alternative Medicine'},
                        {name: 'Nursing - A&E, Critical Care & ICU'},
                        {name: 'Nursing - Aged Care'},
                        {name: 'Nursing - Community, Maternal & Child Health'},
                        {name: 'Nursing - Educators & Facilitators'},
                        {name: 'Nursing - General Medical & Surgical'},
                        {name: 'Nursing - High Acuity'},
                        {name: 'Nursing - Management'},
                        {name: 'Nursing - Midwifery, Neo-Natal, SCN & NICU'},
                        {name: 'Nursing - Paediatric & PICU'},
                        {name: 'Nursing - Psych, Forensic & Correctional Health'},
                        {name: 'Nursing - Theatre & Recovery'},
                        {name: 'Optical'},
                        {name: 'Pathology'},
                        {name: 'Pharmaceuticals & Medical Devices'},
                        {name: 'Pharmacy'},
                        {name: 'Psychology, Counselling & Social Work'},
                        {name: 'Residents & Registrars'},
                        {name: 'Sales'},
                        {name: 'Speech Therapy'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Hospitality & Tourism',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Hospitality & Tourism'},
                        {name: 'Airlines'},
                        {name: 'Bar & Beverage Staff'},
                        {name: 'Chefs/Cooks'},
                        {name: 'Front Office & Guest Services'},
                        {name: 'Gaming'},
                        {name: 'Housekeeping'},
                        {name: 'Kitchen & Sandwich Hands'},
                        {name: 'Management'},
                        {name: 'Reservations'},
                        {name: 'Tour Guides'},
                        {name: 'Travel Agents/Consultants'},
                        {name: 'Waiting Staff'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Human Resources & Recruitment',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Human Resources & Recruitment'},
                        {name: 'Consulting & Generalist HR'},
                        {name: 'Industrial & Employee Relations'},
                        {name: 'Management - Agency'},
                        {name: 'Management - Internal'},
                        {name: 'Occupational Health & Safety'},
                        {name: 'Organisational Development'},
                        {name: 'Recruitment - Agency'},
                        {name: 'Recruitment - Internal'},
                        {name: 'Remuneration & Benefits'},
                        {name: 'Training & Development'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Information & Communication Technology',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Information & Communication Technology'},
                        {name: 'Architects'},
                        {name: 'Business/Systems Analysts'},
                        {name: 'Computer Operators'},
                        {name: 'Consultants'},
                        {name: 'Database Development & Administration'},
                        {name: 'Developers/Programmers'},
                        {name: 'Engineering - Hardward'},
                        {name: 'Engineering - Network'},
                        {name: 'Engineering - Software'},
                        {name: 'Help Desk & IT Support'},
                        {name: 'Management'},
                        {name: 'Networks & Systems Administration'},
                        {name: 'Product Management & Development'},
                        {name: 'Programme & Project Management'},
                        {name: 'Sales - Pre & Post'},
                        {name: 'Security'},
                        {name: 'Team Leaders'},
                        {name: 'Technical Writing'},
                        {name: 'Telecommunications'},
                        {name: 'Testing & Quality Assurance'},
                        {name: 'Web Development & Production'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Insurance & Superannuation',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Insurance & Superannuation'},
                        {name: 'Actuarial'},
                        {name: 'Assessment'},
                        {name: 'Brokerage'},
                        {name: 'Claims'},
                        {name: 'Fund Administration'},
                        {name: 'Management'},
                        {name: 'Risk Consulting'},
                        {name: 'Superannuation'},
                        {name: 'Underwriting'},
                        {name: 'Workers Compensation'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Legal',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Legal'},
                        {name: 'Banking & Finance Law'},
                        {name: 'Construction Law'},
                        {name: 'Corporate & Commercial Law'},
                        {name: 'Criminal & Civil Law'},
                        {name: 'Environment & Planning Law'},
                        {name: 'Family Law'},
                        {name: 'Generalists - In-house'},
                        {name: 'Generalists - Law Firm'},
                        {name: 'Industrial Relations & Employment Law'},
                        {name: 'Insurance & Superannuation Law'},
                        {name: 'Intellectual Property Law'},
                        {name: 'Law Clerks & Paralegals'},
                        {name: 'Legal Practice Management'},
                        {name: 'Legal Secretaries'},
                        {name: 'Litigation & Dispute Resolution'},
                        {name: 'Personal Injury Law'},
                        {name: 'Property Law'},
                        {name: 'Tax Law'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Manufacturing, Transport & Logistics',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Manufacturing, Transport & Logistics'},
                        {name: 'Analysis & Reporting'},
                        {name: 'Assembly & Process Work'},
                        {name: 'Aviation Services'},
                        {name: 'Couriers, Drivers & Postal Services'},
                        {name: 'Fleet Management'},
                        {name: 'Freight/Cargo Forwarding'},
                        {name: 'Import/Export & Customs'},
                        {name: 'Machine Operators'},
                        {name: 'Management'},
                        {name: 'Pattern Makers & Garment Technicians'},
                        {name: 'Pickers & Packers'},
                        {name: 'Production, Planning & Scheduling'},
                        {name: 'Public Transport & Taxi Services'},
                        {name: 'Purchasing, Procurement & Inventory'},
                        {name: 'Quality Assurance & Control'},
                        {name: 'Rail & Maritime Transport'},
                        {name: 'Road Transport'},
                        {name: 'Team Leaders/Supervisors'},
                        {name: 'Warehousing, Storage & Distribution'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Marketing & Communications',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Marketing & Communications'},
                        {name: 'Brand Management'},
                        {name: 'Digital & Search Marketing'},
                        {name: 'Direct Marketing & CRM'},
                        {name: 'Event Management'},
                        {name: 'Internal Communications'},
                        {name: 'Management'},
                        {name: 'Market Research & Analysis'},
                        {name: 'Marketing Assistants/Coordinators'},
                        {name: 'Marketing Communications'},
                        {name: 'Product Management & Development'},
                        {name: 'Public Relations & Corporate Affairs'},
                        {name: 'Trade Marketing'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Mining, Resources & Energy',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Mining, Resources & Energy'},
                        {name: 'Analysis & Reporting'},
                        {name: 'Health, Safety & Environment'},
                        {name: 'Management'},
                        {name: 'Mining - Drill & Blast'},
                        {name: 'Mining - Engineering & Maintenance'},
                        {name: 'Mining - Exploration & Geoscience'},
                        {name: 'Mining - Operations'},
                        {name: 'Mining - Processing'},
                        {name: 'Natural Resources & Water'},
                        {name: 'Oil & Gas - Drilling'},
                        {name: 'Oil & Gas - Engineering & Maintenance'},
                        {name: 'Oil & Gas - Exploration & Geoscience'},
                        {name: 'Oil & Gas - Operations'},
                        {name: 'Oil & Gas - Production & Refinement'},
                        {name: 'Power Generation & Distribution'},
                        {name: 'Surveying'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Real Estate & Property',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Real Estate & Property'},
                        {name: 'Administration'},
                        {name: 'Analysts'},
                        {name: 'Body Corporate & Facilities Management'},
                        {name: 'Commercial Sales, Leasing & Property Mgmt'},
                        {name: 'Residential Leasing & Property Management'},
                        {name: 'Residential Sales'},
                        {name: 'Retail & Property Development'},
                        {name: 'Valuation'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Retail & Consumer Products',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Retail & Consumer Products'},
                        {name: 'Buying'},
                        {name: 'Management - Area/Multi-site'},
                        {name: 'Management - Department/Assistant'},
                        {name: 'Management - Store'},
                        {name: 'Merchandisers'},
                        {name: 'Planning'},
                        {name: 'Retail Assistants'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Sales',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Sales'},
                        {name: 'Account & Relationship Management'},
                        {name: 'Analysis & Reporting'},
                        {name: 'Management'},
                        {name: 'New Business Development'},
                        {name: 'Sales Coordinators'},
                        {name: 'Sales Representatives/Consultants'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Science & Technology',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Science & Technology'},
                        {name: 'Biological & Biomedical Sciences'},
                        {name: 'Biotechnology & Genetics'},
                        {name: 'Chemistry & Physics'},
                        {name: 'Environmental, Earth & Geosciences'},
                        {name: 'Food Technology & Safety'},
                        {name: 'Laboratory & Technical Services'},
                        {name: 'Materials Sciences'},
                        {name: 'Mathematics, Statistics & Information Sciences'},
                        {name: 'Modelling & Simulation'},
                        {name: 'Quality Assurance & Control'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Sport & Recreation',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Sport & Recreation'},
                        {name: 'Coaching & Instruction'},
                        {name: 'Fitness & Personal Training'},
                        {name: 'Management'},
                    ]
                }
            }
        }
    });

    await prisma.industry.create({
        data: {
            name: 'Trades & Services',
            professions: {
                createMany: {
                    data: [
                        {name: 'All Trades & Services'},
                        {name: 'Air Conditioning & Refrigeration'},
                        {name: 'Automotive Trades'},
                        {name: 'Bakers & Pastry Chefs'},
                        {name: 'Building Trades'},
                        {name: 'Butchers'},
                        {name: 'Carpentry & Cabinet Making'},
                        {name: 'Cleaning Services'},
                        {name: 'Electricians'},
                        {name: 'Fitters, Turners & Machinists'},
                        {name: 'Floristry'},
                        {name: 'Gardening & Landscaping'},
                        {name: 'Hair & Beauty Services'},
                        {name: 'Labourers'},
                        {name: 'Locksmiths'},
                        {name: 'Maintenance & Handyperson Services'},
                        {name: 'Nannies & Babysitters'},
                        {name: 'Painters & Sign Writers'},
                        {name: 'Plumbers'},
                        {name: 'Printing & Publishing Services'},
                        {name: 'Security Services'},
                        {name: 'Tailors & Dressmakers'},
                        {name: 'Technicians'},
                        {name: 'Welders & Boilermakers'},
                    ]
                }
            }
        }
    });

    //insert resume and snippets categories
    //Insert primary skills
    /*
    soft skills: time management, communication, adaptability, problem solving, teamwork,
    creativity, leadership, interpersonal skill, work ethic, attention to detail, collaboration

    hard skills: computer technology, data analysis, math, seo, marketing, project management, design, cloud computing,
    mobile development, programing languages
    * */

    // await prisma.snippet.createMany({
    //     data: [
    //         {
    //             type: "resume",
    //             segment: "primary_skills",
    //             body: {
    //                 suggest: "Interpersonal skills",
    //             }
    //         }
    //     ]
    // })
}

main()
    .catch((e) => {
        console.error(e)
    })
    .finally(async () => {
        await prisma.$disconnect()
    })