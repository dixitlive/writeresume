/*
  Warnings:

  - Made the column `userId` on table `CoverLetter` required. This step will fail if there are existing NULL values in that column.
  - Made the column `userId` on table `Resume` required. This step will fail if there are existing NULL values in that column.
  - Made the column `categoryId` on table `Snippet` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "CoverLetter" DROP CONSTRAINT "CoverLetter_userId_fkey";

-- DropForeignKey
ALTER TABLE "Resume" DROP CONSTRAINT "Resume_userId_fkey";

-- DropForeignKey
ALTER TABLE "Snippet" DROP CONSTRAINT "Snippet_categoryId_fkey";

-- AlterTable
ALTER TABLE "CoverLetter" ALTER COLUMN "body" DROP NOT NULL,
ALTER COLUMN "userId" SET NOT NULL;

-- AlterTable
ALTER TABLE "Resume" ALTER COLUMN "userId" SET NOT NULL,
ALTER COLUMN "body" DROP NOT NULL,
ALTER COLUMN "preference" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Snippet" ALTER COLUMN "categoryId" SET NOT NULL,
ALTER COLUMN "body" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "Resume" ADD CONSTRAINT "Resume_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CoverLetter" ADD CONSTRAINT "CoverLetter_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Snippet" ADD CONSTRAINT "Snippet_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
