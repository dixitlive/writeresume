/*
  Warnings:

  - You are about to drop the `Category` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_CategoryToResume` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_CategoryToSnippet` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_CategoryToResume" DROP CONSTRAINT "_CategoryToResume_A_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToResume" DROP CONSTRAINT "_CategoryToResume_B_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToSnippet" DROP CONSTRAINT "_CategoryToSnippet_A_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToSnippet" DROP CONSTRAINT "_CategoryToSnippet_B_fkey";

-- AlterTable
ALTER TABLE "CoverLetter" ADD COLUMN     "preference" JSONB;

-- DropTable
DROP TABLE "Category";

-- DropTable
DROP TABLE "_CategoryToResume";

-- DropTable
DROP TABLE "_CategoryToSnippet";

-- CreateTable
CREATE TABLE "Industry" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "archivedAt" TIMESTAMP(3),

    CONSTRAINT "Industry_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Profession" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "archivedAt" TIMESTAMP(3),

    CONSTRAINT "Profession_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_IndustryToSnippet" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_ProfessionToResume" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_IndustryToSnippet_AB_unique" ON "_IndustryToSnippet"("A", "B");

-- CreateIndex
CREATE INDEX "_IndustryToSnippet_B_index" ON "_IndustryToSnippet"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_ProfessionToResume_AB_unique" ON "_ProfessionToResume"("A", "B");

-- CreateIndex
CREATE INDEX "_ProfessionToResume_B_index" ON "_ProfessionToResume"("B");

-- AddForeignKey
ALTER TABLE "_IndustryToSnippet" ADD CONSTRAINT "_IndustryToSnippet_A_fkey" FOREIGN KEY ("A") REFERENCES "Industry"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_IndustryToSnippet" ADD CONSTRAINT "_IndustryToSnippet_B_fkey" FOREIGN KEY ("B") REFERENCES "Snippet"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ProfessionToResume" ADD CONSTRAINT "_ProfessionToResume_A_fkey" FOREIGN KEY ("A") REFERENCES "Profession"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ProfessionToResume" ADD CONSTRAINT "_ProfessionToResume_B_fkey" FOREIGN KEY ("B") REFERENCES "Resume"("id") ON DELETE CASCADE ON UPDATE CASCADE;
