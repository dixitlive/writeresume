/*
  Warnings:

  - You are about to drop the `_CategoryToSnippet` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_CategoryToSnippet" DROP CONSTRAINT "_CategoryToSnippet_A_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToSnippet" DROP CONSTRAINT "_CategoryToSnippet_B_fkey";

-- DropTable
DROP TABLE "_CategoryToSnippet";

-- CreateTable
CREATE TABLE "CategoriesOnSnippets" (
    "snippetId" TEXT NOT NULL,
    "categoryId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "CategoriesOnSnippets_pkey" PRIMARY KEY ("snippetId","categoryId")
);

-- AddForeignKey
ALTER TABLE "CategoriesOnSnippets" ADD CONSTRAINT "CategoriesOnSnippets_snippetId_fkey" FOREIGN KEY ("snippetId") REFERENCES "Snippet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CategoriesOnSnippets" ADD CONSTRAINT "CategoriesOnSnippets_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
