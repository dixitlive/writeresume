/*
  Warnings:

  - You are about to drop the column `categoryId` on the `Profession` table. All the data in the column will be lost.
  - You are about to drop the `CategoriesOnSnippets` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Category` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `industryId` to the `Profession` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "CategoriesOnSnippets" DROP CONSTRAINT "CategoriesOnSnippets_categoryId_fkey";

-- DropForeignKey
ALTER TABLE "CategoriesOnSnippets" DROP CONSTRAINT "CategoriesOnSnippets_snippetId_fkey";

-- DropForeignKey
ALTER TABLE "Profession" DROP CONSTRAINT "Profession_categoryId_fkey";

-- AlterTable
ALTER TABLE "Profession" DROP COLUMN "categoryId",
ADD COLUMN     "industryId" TEXT NOT NULL;

-- DropTable
DROP TABLE "CategoriesOnSnippets";

-- DropTable
DROP TABLE "Category";

-- CreateTable
CREATE TABLE "Industry" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "archivedAt" TIMESTAMP(3),

    CONSTRAINT "Industry_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "IndustriesOnSnippets" (
    "industryId" TEXT NOT NULL,
    "snippetId" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "IndustriesOnSnippets_pkey" PRIMARY KEY ("industryId","snippetId")
);

-- AddForeignKey
ALTER TABLE "Profession" ADD CONSTRAINT "Profession_industryId_fkey" FOREIGN KEY ("industryId") REFERENCES "Industry"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "IndustriesOnSnippets" ADD CONSTRAINT "IndustriesOnSnippets_industryId_fkey" FOREIGN KEY ("industryId") REFERENCES "Industry"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "IndustriesOnSnippets" ADD CONSTRAINT "IndustriesOnSnippets_snippetId_fkey" FOREIGN KEY ("snippetId") REFERENCES "Snippet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
