/*
  Warnings:

  - You are about to drop the column `deletedAt` on the `Resume` table. All the data in the column will be lost.
  - You are about to drop the column `deleteddAt` on the `Snippet` table. All the data in the column will be lost.
  - You are about to drop the column `updateddAt` on the `Snippet` table. All the data in the column will be lost.
  - You are about to drop the `_CategoryToResume` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `archivedAt` to the `Resume` table without a default value. This is not possible if the table is not empty.
  - Added the required column `body` to the `Resume` table without a default value. This is not possible if the table is not empty.
  - Added the required column `preference` to the `Resume` table without a default value. This is not possible if the table is not empty.
  - Changed the type of `body` on the `Snippet` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- DropForeignKey
ALTER TABLE "Resume" DROP CONSTRAINT "Resume_userId_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToResume" DROP CONSTRAINT "_CategoryToResume_A_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToResume" DROP CONSTRAINT "_CategoryToResume_B_fkey";

-- AlterTable
ALTER TABLE "Resume" DROP COLUMN "deletedAt",
ADD COLUMN     "archivedAt" TIMESTAMP(3) NOT NULL,
ADD COLUMN     "body" JSONB NOT NULL,
ADD COLUMN     "categoryId" INTEGER,
ADD COLUMN     "preference" JSONB NOT NULL,
ALTER COLUMN "userId" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Snippet" DROP COLUMN "deleteddAt",
DROP COLUMN "updateddAt",
ADD COLUMN     "archivedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN     "categoryId" INTEGER,
ADD COLUMN     "updatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
DROP COLUMN "body",
ADD COLUMN     "body" JSONB NOT NULL;

-- DropTable
DROP TABLE "_CategoryToResume";

-- CreateTable
CREATE TABLE "CoverLetter" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "body" JSONB NOT NULL,
    "userId" INTEGER,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "archivedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "CoverLetter_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Resume" ADD CONSTRAINT "Resume_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Resume" ADD CONSTRAINT "Resume_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CoverLetter" ADD CONSTRAINT "CoverLetter_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Snippet" ADD CONSTRAINT "Snippet_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE SET NULL ON UPDATE CASCADE;
