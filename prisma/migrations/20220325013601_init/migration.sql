-- AlterTable
ALTER TABLE "Category" ALTER COLUMN "archivedAt" DROP NOT NULL;

-- AlterTable
ALTER TABLE "CoverLetter" ALTER COLUMN "archivedAt" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Resume" ALTER COLUMN "archivedAt" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Snippet" ALTER COLUMN "archivedAt" DROP NOT NULL,
ALTER COLUMN "archivedAt" DROP DEFAULT,
ALTER COLUMN "updatedAt" DROP DEFAULT;
