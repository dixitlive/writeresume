/*
  Warnings:

  - You are about to drop the column `names` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "names",
ADD COLUMN     "name" TEXT;
