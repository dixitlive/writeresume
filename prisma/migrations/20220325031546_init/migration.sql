/*
  Warnings:

  - You are about to drop the column `categoryId` on the `Resume` table. All the data in the column will be lost.
  - You are about to drop the column `categoryId` on the `Snippet` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Resume" DROP CONSTRAINT "Resume_categoryId_fkey";

-- DropForeignKey
ALTER TABLE "Snippet" DROP CONSTRAINT "Snippet_categoryId_fkey";

-- AlterTable
ALTER TABLE "Resume" DROP COLUMN "categoryId";

-- AlterTable
ALTER TABLE "Snippet" DROP COLUMN "categoryId";

-- CreateTable
CREATE TABLE "_CategoryToResume" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_CategoryToSnippet" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_CategoryToResume_AB_unique" ON "_CategoryToResume"("A", "B");

-- CreateIndex
CREATE INDEX "_CategoryToResume_B_index" ON "_CategoryToResume"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_CategoryToSnippet_AB_unique" ON "_CategoryToSnippet"("A", "B");

-- CreateIndex
CREATE INDEX "_CategoryToSnippet_B_index" ON "_CategoryToSnippet"("B");

-- AddForeignKey
ALTER TABLE "_CategoryToResume" ADD FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToResume" ADD FOREIGN KEY ("B") REFERENCES "Resume"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSnippet" ADD FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSnippet" ADD FOREIGN KEY ("B") REFERENCES "Snippet"("id") ON DELETE CASCADE ON UPDATE CASCADE;
