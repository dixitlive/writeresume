/*
  Warnings:

  - You are about to drop the column `assignedAt` on the `CategoriesOnSnippets` table. All the data in the column will be lost.
  - You are about to drop the column `assignedBy` on the `CategoriesOnSnippets` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "CategoriesOnSnippets" DROP COLUMN "assignedAt",
DROP COLUMN "assignedBy",
ADD COLUMN     "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP;
