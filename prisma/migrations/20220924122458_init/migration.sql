/*
  Warnings:

  - You are about to drop the `Industry` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_IndustryToSnippet` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_ProfessionToResume` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `professionId` to the `CoverLetter` table without a default value. This is not possible if the table is not empty.
  - Added the required column `categoryId` to the `Profession` table without a default value. This is not possible if the table is not empty.
  - Added the required column `professionId` to the `Resume` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "_IndustryToSnippet" DROP CONSTRAINT "_IndustryToSnippet_A_fkey";

-- DropForeignKey
ALTER TABLE "_IndustryToSnippet" DROP CONSTRAINT "_IndustryToSnippet_B_fkey";

-- DropForeignKey
ALTER TABLE "_ProfessionToResume" DROP CONSTRAINT "_ProfessionToResume_A_fkey";

-- DropForeignKey
ALTER TABLE "_ProfessionToResume" DROP CONSTRAINT "_ProfessionToResume_B_fkey";

-- AlterTable
ALTER TABLE "CoverLetter" ADD COLUMN     "professionId" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Profession" ADD COLUMN     "categoryId" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Resume" ADD COLUMN     "professionId" TEXT NOT NULL;

-- DropTable
DROP TABLE "Industry";

-- DropTable
DROP TABLE "_IndustryToSnippet";

-- DropTable
DROP TABLE "_ProfessionToResume";

-- CreateTable
CREATE TABLE "Category" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "archivedAt" TIMESTAMP(3),

    CONSTRAINT "Category_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CategoriesOnSnippets" (
    "snippetId" TEXT NOT NULL,
    "categoryId" TEXT NOT NULL,
    "assignedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "assignedBy" TEXT NOT NULL,

    CONSTRAINT "CategoriesOnSnippets_pkey" PRIMARY KEY ("snippetId","categoryId")
);

-- AddForeignKey
ALTER TABLE "Profession" ADD CONSTRAINT "Profession_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Resume" ADD CONSTRAINT "Resume_professionId_fkey" FOREIGN KEY ("professionId") REFERENCES "Profession"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CoverLetter" ADD CONSTRAINT "CoverLetter_professionId_fkey" FOREIGN KEY ("professionId") REFERENCES "Profession"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CategoriesOnSnippets" ADD CONSTRAINT "CategoriesOnSnippets_snippetId_fkey" FOREIGN KEY ("snippetId") REFERENCES "Snippet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CategoriesOnSnippets" ADD CONSTRAINT "CategoriesOnSnippets_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
