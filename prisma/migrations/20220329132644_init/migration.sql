/*
  Warnings:

  - The primary key for the `Category` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `CoverLetter` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Resume` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Snippet` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- DropForeignKey
ALTER TABLE "_CategoryToResume" DROP CONSTRAINT "_CategoryToResume_A_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToResume" DROP CONSTRAINT "_CategoryToResume_B_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToSnippet" DROP CONSTRAINT "_CategoryToSnippet_A_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToSnippet" DROP CONSTRAINT "_CategoryToSnippet_B_fkey";

-- AlterTable
ALTER TABLE "Category" DROP CONSTRAINT "Category_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Category_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Category_id_seq";

-- AlterTable
ALTER TABLE "CoverLetter" DROP CONSTRAINT "CoverLetter_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "CoverLetter_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "CoverLetter_id_seq";

-- AlterTable
ALTER TABLE "Resume" DROP CONSTRAINT "Resume_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Resume_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Resume_id_seq";

-- AlterTable
ALTER TABLE "Snippet" DROP CONSTRAINT "Snippet_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Snippet_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "Snippet_id_seq";

-- AlterTable
ALTER TABLE "_CategoryToResume" ALTER COLUMN "A" SET DATA TYPE TEXT,
ALTER COLUMN "B" SET DATA TYPE TEXT;

-- AlterTable
ALTER TABLE "_CategoryToSnippet" ALTER COLUMN "A" SET DATA TYPE TEXT,
ALTER COLUMN "B" SET DATA TYPE TEXT;

-- AddForeignKey
ALTER TABLE "_CategoryToResume" ADD FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToResume" ADD FOREIGN KEY ("B") REFERENCES "Resume"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSnippet" ADD FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSnippet" ADD FOREIGN KEY ("B") REFERENCES "Snippet"("id") ON DELETE CASCADE ON UPDATE CASCADE;
