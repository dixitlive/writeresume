/*
  Warnings:

  - You are about to drop the `CategoriesOnSnippets` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "CategoriesOnSnippets" DROP CONSTRAINT "CategoriesOnSnippets_categoryId_fkey";

-- DropForeignKey
ALTER TABLE "CategoriesOnSnippets" DROP CONSTRAINT "CategoriesOnSnippets_snippetId_fkey";

-- DropTable
DROP TABLE "CategoriesOnSnippets";

-- CreateTable
CREATE TABLE "_CategoryToSnippet" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_CategoryToSnippet_AB_unique" ON "_CategoryToSnippet"("A", "B");

-- CreateIndex
CREATE INDEX "_CategoryToSnippet_B_index" ON "_CategoryToSnippet"("B");

-- AddForeignKey
ALTER TABLE "_CategoryToSnippet" ADD CONSTRAINT "_CategoryToSnippet_A_fkey" FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSnippet" ADD CONSTRAINT "_CategoryToSnippet_B_fkey" FOREIGN KEY ("B") REFERENCES "Snippet"("id") ON DELETE CASCADE ON UPDATE CASCADE;
