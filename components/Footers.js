import { Layout } from 'antd';
const { Footer } = Layout;

const Footers = () => {
    return (
        <Footer style={{ textAlign: 'center' }}>
            WriteResume ©2022 Created by Dixit Patel.
        </Footer>
    )
}

export default Footers;