import Link from 'next/link'
//import { useRouter } from 'next/router';
import { Layout, Menu } from 'antd';
const { Header } = Layout;
import { EditOutlined, UserOutlined, LoginOutlined, MenuOutlined } from '@ant-design/icons';
//import Image from 'next/image'

const TopNav = () => {

    // const router = useRouter();
    // const router = useRouter();
    // className={router.pathname == "/pricing" ? "active" : ""}

    const items = [
        { icon: <EditOutlined />, label: <a href="/write-resume"> Start Writing Resume </a>, key: 'item-1' },
        { icon: <EditOutlined />, label: <a href="/resumes"> Resumes </a>, key: 'item-2' },
        { icon: <EditOutlined />, label: <a href="/about" > About </a>, key: 'item-3' },
        { icon: <EditOutlined />, label: <a href="/pricing" > Pricing  </a>, key: 'item-4' },
        { icon: <UserOutlined />, label: <a href="/profile"> Profile </a>, key: 'item-5' },
        { icon: <EditOutlined />, label: <a href="/subscription"> Subscription </a>, key: 'item-6' },
        { icon: <LoginOutlined />, label: <a href="/login"> Login </a>, key: 'item-7' },
    ];

    return (
        <Header className="header" style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
            <div className="logo" />
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['write-resume']} overflowedIndicator={<MenuOutlined />} items={items}></Menu>
        </Header >
    )
}

export default TopNav;