import TopNav from './TopNav'
import Footers from './Footers'
import { Layout } from 'antd';
const { Content } = Layout;
import { useSession, signIn, signOut } from "next-auth/react"

const Main = ({ children }) => {
    return (
        <Layout>
            <TopNav />
            <Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
                <div className="site-layout-background" style={{ padding: 24, minHeight: 380 }}>
                    {children}
                </div>
            </Content>
            <Footers />
        </Layout>
    )
}

export default Main