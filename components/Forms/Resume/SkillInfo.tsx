import {Row, Col, Card} from 'antd';
import {HolderOutlined} from "@ant-design/icons";
import { useForm, SubmitHandler } from "react-hook-form";
import { Select } from 'antd';

type Inputs = {
    softSkills: string,
    hardSkills: string,
    interests: string,
}

function SkillInfo({ resume, saveResume, isSaving }){

    const { register, setValue, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        saveResume(data);
    }

    const { Option } = Select;
    const softSkills = [];
    const hardSkills = [];
    const interests = [];

    for (let i = 10; i < 36; i++) {
        softSkills.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
        hardSkills.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
        interests.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
    }

    const softSkillChange = (value) => {
        setValue("softSkills", value)
        console.log(`softSkills ${value}`);
    };

    const hardSkillChange = (value) => {
        setValue("hardSkills", value)
        console.log(`hardSkills ${value}`);
    };

    const interestsChange = (value) => {
        setValue("interests", value)
        console.log(`interests ${value}`);
    };

    return (
        <>
            <Row>
                <Col span={12} offset={6}>
                    <Card title="Skills" extra={<a href="#"><HolderOutlined /></a>}>

                        <form onSubmit={handleSubmit(onSubmit)}>

                            <input hidden={true} {...register("softSkills")} />
                            <input hidden={true} {...register("hardSkills")} />

                            {/*organisational skills (soft skills), languages (soft skills), technical skills (hard skill), software skills (hard skill), additional interests (any) */}
                            <Row>
                                <Col span={24}>
                                    <label htmlFor={"softSkills"}>Life skills (soft skills)</label>
                                    <Select
                                        mode="tags"
                                        id="softSkills"
                                        style={{
                                            width: '100%',
                                        }}
                                        placeholder="Type or select"
                                        onChange={softSkillChange}
                                    >
                                        {softSkills}
                                    </Select>
                                </Col>
                            </Row>

                            <Row>
                                <Col span={24}>
                                    <label htmlFor={"hardSkills"}>Technical skills (hard skills)</label>
                                    <Select
                                        mode="tags"
                                        id="hardSkills"
                                        style={{
                                            width: '100%',
                                        }}
                                        placeholder="Type or select"
                                        onChange={hardSkillChange}
                                    >
                                        {hardSkills}
                                    </Select>
                                </Col>
                            </Row>

                            <Row>
                                <Col span={24}>
                                    <label htmlFor={"interests"}>Personal interests (personality traits)</label>
                                    <Select
                                        mode="tags"
                                        id="interests"
                                        style={{
                                            width: '100%',
                                        }}
                                        placeholder="Type or select"
                                        onChange={interestsChange}
                                    >
                                        {interests}
                                    </Select>
                                </Col>
                            </Row>

                            <button type={"submit"}>Save</button>
                        </form>
                    </Card>
                </Col>
            </Row>
        </>
    );
}

export default SkillInfo;