import {Row, Col, Card} from 'antd';
import Tiptap from '../../Tiptap'
import React, { useState, useEffect } from "react";
import {HolderOutlined} from "@ant-design/icons";
import { useForm, SubmitHandler } from "react-hook-form";

type Inputs = {
    carrierSummary: string,
}

function CarrierSummary({ resume, saveResume, isSaving }){

    const { register, setValue, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        saveResume(data);
    }

    //setter
    const [summary, setSummary] = useState(resume?.hero_info?.carrier_summary);

    const getSummary = () => {
        setValue("carrierSummary", summary)
    }

    //getter
    useEffect(getSummary, [summary]);

    return (
        <>
            <Row>
                <Col span={12} offset={6}>
                    <Card title="Carrier Summary" extra={<a href="#"><HolderOutlined /></a>}>

                        <form onSubmit={handleSubmit(onSubmit)}>

                            <input hidden={true} {...register("carrierSummary")} />

                            <Row>
                                <Col span={24}>
                                    <label htmlFor={"CarrierSummary"}>Carrier Summary</label>
                                    <Tiptap htmlData={summary} setHtmlData={setSummary} />
                                </Col>
                            </Row>

                            <button type={"submit"}>Save</button>
                        </form>
                    </Card>
                </Col>
            </Row>
        </>
    );
}

export default CarrierSummary;