import {Row, Col, Card} from 'antd';
import {HolderOutlined} from "@ant-design/icons";
import { useForm, SubmitHandler } from "react-hook-form";
import { Select } from 'antd';

type Inputs = {
    industryId: string,
}

function Settings({ resume, saveResume, isSaving, data }){

    const { register, setValue, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        saveResume(data);
    }

    const { Option } = Select;

    const industryChange = (value) => {
        setValue("industryId", value.id)
        console.log(`industryId ${value}`);
    };

    const onSearch = (value) => {
        console.log('search:', value);
    };

    return (
        <>
            <Row>
                <Col span={12} offset={6}>
                    <Card title="Settings" extra={<a href="#"><HolderOutlined /></a>}>

                        <form onSubmit={handleSubmit(onSubmit)}>

                            <input hidden={true} {...register("industryId")} />

                            <Row>
                                <Col span={24}>
                                    <label>Industry</label>
                                    <Select
                                        showSearch
                                        id="profession"
                                        style={{
                                            width: '100%',
                                        }}
                                        placeholder="Type or select"
                                        optionFilterProp="children"
                                        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
                                        onSearch={onSearch}
                                        onChange={industryChange}
                                    >
                                        {
                                            data.industries.map( (v, k) =>
                                                <option key={v.id}>{v.name}</option> )
                                        }
                                    </Select>
                                </Col>
                            </Row>

                            <button type={"submit"}>Save</button>
                        </form>
                    </Card>
                </Col>
            </Row>
        </>
    );
}

export default Settings;