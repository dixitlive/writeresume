import { Row, Col, Card } from 'antd';
import { HolderOutlined } from '@ant-design/icons';
import { useForm, SubmitHandler } from "react-hook-form";

type Inputs = {
    title: string,
    firstName: string,
    lastName: string,
    email: string,
    mobile: string,
    unitNumber: string,
    streetNumber: string,
    streetName: string,
    suburb: string,
    postcode: string,
    country: string
}

function ContactInfo({ resume, saveResume, isSaving }) {

    const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        saveResume(data);
    }

    return (
        <>
            <Row>
                <Col span={12} offset={6}>
                    <Card title="Contact Information" extra={<a href="#"><HolderOutlined /></a>}>

                        <form onSubmit={handleSubmit(onSubmit)}>
                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Title</label>
                                    <input defaultValue={resume.title} {...register("title", { required: 'Please provide title' })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>First name</label>
                                    <input defaultValue={resume.firstName}  {...register("firstName", { required: 'Please provide first name' })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Last name</label>
                                    <input defaultValue={resume.lastName} {...register("lastName", { required: 'Please provide last name' })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Email</label>
                                    <input defaultValue={resume.email} {...register("email", { required: false })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Mobile</label>
                                    <input defaultValue={resume.mobile} {...register("mobile", { required: false })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Unit number</label>
                                    <input defaultValue={resume.unitNumber} {...register("unitNumber", { required: false })} />
                                </Col>
                            </Row>

                             <Row gutter={16}>
                                <Col span={24}>
                                    <label>Street number</label>
                                    <input defaultValue={resume.streetNumber} {...register("streetNumber", { required: false })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Street name</label>
                                    <input defaultValue={resume.streetName} {...register("streetName", { required: false })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Suburb</label>
                                    <input defaultValue={resume.suburb} {...register("suburb", { required: false })} />
                                </Col>
                            </Row>

                            <Row gutter={16}>
                                <Col span={24}>
                                    <label>Postcode</label>
                                    <input defaultValue={resume.postcode} {...register("postcode", { required: false })} />
                                </Col>
                            </Row>

                            <select defaultValue={resume.country} {...register("country")}>
                                <option value="australia">Australia</option>
                                <option value="new-zealand">New Zealand</option>
                            </select>

                            <button type={"submit"}>Save</button>
                        </form>

                    </Card>
                </Col>
            </Row>
        </>
    );
}

export default ContactInfo;