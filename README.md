## System Requirement
- [Nodejs](https://nodejs.org/en/)
- [PostgreSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads)
- [NextJS](https://nextjs.org/docs)

## Type of CLIs to run commands
- Next cli
- Prisma cli

## Getting Started
- Install node dependacy: 'npm install'
- Database setup: open pgAdmin, create new database 'writeresume' then run 'npx prisma migrate dev'
- Database see: npx prisma db seed
- Run web server: 'npm run dev'
- Open [http://localhost:3000](http://localhost:3000) with your browser to see the result
- [API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello]
- Prisma CLI commands list: npx prisma 
- Nextjs CLI command list: npx next -h
- To view data in prisma studio: npx prisma studio then open [http://localhost:5555/](http://localhost:5555/)
- To view data in pgAdmin, open pgAdmin client
