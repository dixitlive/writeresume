import Head from 'next/head'
import Link from 'next/link'

/** @param {import('next').InferGetServerSidePropsType<typeof getServerSideProps> } props */
export default function Resumes({ resumes }) {
    return (
        <div>
            <Head>
                <title>My Resumes</title>
            </Head>
            <h1>
                My Resumes
            </h1>
            {resumes?.map((item, i) => (
                <li key="{item.id}">
                    <Link
                        href={{
                            pathname: '/resumes/preview',
                            query: { hash: item.id },
                        }}
                    >
                        {item.title}
                    </Link>
                </li>
            ))}
        </div>
    )
}

export async function getServerSideProps(context) {

    const { NEXT_PUBLIC_URL } = process.env;

    // Fetch data from API
    const request = await fetch(NEXT_PUBLIC_URL + '/api/resumes/list', {
        headers: {
            cookie: context.req.headers.cookie || "",
        }
    });

    const result = await request.json();
    return { props: { resumes: result.data.resumes } };
}
