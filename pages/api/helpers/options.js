import { getSession } from "next-auth/react"
import prisma from "../../../lib/prisma";

export default async (req, res) => {

    const session = await getSession({ req });
    if (!session || !session.user) {
        result.message = "Unauthorized";
        return res.status(401).json(result);
    }

    let industries;
    let result = { data: { }, message: "" };

    try {
        industries = await prisma.industry.findMany();
    } catch (error) {
        result.message = "Something went wrong";
        return res.status(400).json(result);
    }

    result.data = { industries };
    result.message = "Success";
    return res.status(200).json(result);
}