import prisma from "../../../lib/prisma";

export default async (req: any, response: any) => {

    let formData = req.body;
    let resume: any = null;
    let user: any = null;
    let profession: any = null;
    let result = { data: {}, message: "" };

    if (req.method !== 'POST') {
        result.message = "Method not allowed";
        return response.status(405).json(result);
    }
    console.log('formData', formData);

    //find user
    try {
        user = await prisma.user.findUnique({
            where: {
                id: formData.userId
            },
        });
    } catch (error) {
        result.message = "Please login before creating record";
        return response.status(401).json(result);
    }

    //find profession
    profession = await prisma.profession.findUnique({
        where: {
            id: formData.industryId ?? "cl8i2x49m0010y4d2zx613pri"
        },
    });

    //prepare json object to store
    let resumeObject = {
        title: formData.title,
        body: {
            contact_info: {
                first_name: formData.firstName,
                last_name: formData.lastName,
                email: formData.email,
                mobile: formData.mobile,
                unit_number: formData.unitNumber,
                street_number: formData.streetNumber,
                street_name: formData.streetName,
                suburb: formData.suburb,
                state: formData.state,
                postcode: formData.postcode,
                country: formData.country,
            },

            hero_info: {
                carrier_summary: formData.carrierSummary,
                hard_skills: formData.hardSkills,
                soft_skills: formData.softSkills,
                interests: formData.interests,
            },

            qualifications: {
                //add any key value from the ui
                //{qualification name, institution, start, end , description}
            },

            experiences: {
                //add any key value from the ui
                //{job title, employer, city, start, end, description }
            },

            achievements: {
                //add any key value from the ui
                //this could be awards, recognition, any rewards, etc.
                //{title, year, description }
            },

            references: {
                //add any key value from the ui
                //this could be awards, recognition, any rewards, etc.
                //{ name, phone, email, description }
            }

        },

        user: {
            connect: {id: user.id}
        },

        profession: {
            connect: {id: profession?.id}
        }
    }

    if (formData.id) {
        try {
            resume = await prisma.resume.update({
                where: { id: formData.id },
                data: resumeObject
            });
        } catch (error) {
            console.log('error', error);
            result.message = "Error while updating record.";
            return response.status(409).json(result);
        }
    } else {
        try {
            resume = await prisma.resume.create({
                data: resumeObject
            })
        } catch (error) {
            console.log('error', error);
            result.message = "Error while creating record.";
            return response.status(409).json(result);
        }
    }

    result.data = { resume: resume };
    result.message = "Success";
    return response.status(200).json(result);
}