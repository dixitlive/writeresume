import prisma from "../../../lib/prisma";
import { getSession } from 'next-auth/react'
import { NextApiRequest, NextApiResponse } from "next";

export default async (req: NextApiRequest, res: NextApiResponse) => {

    let result = { data: { resume: {} }, message: "" };
    let record = null;

    const session = await getSession({ req });
    if (!session || !session.user) {
        result.message = "Unauthorized"
        return res.status(401).json(result);
    }

    const requestData = req.query;

    try {
        record = await prisma.resume.findUnique({
            where: {
                id: requestData.hash,
            },
        });

        console.log(record);
    } catch (error) {
        result.message = "Something went wrong";
        return res.status(400).json(result);
    }

    result.data.resume = record;
    result.message = "Success";
    return res.status(200).json(result);
}