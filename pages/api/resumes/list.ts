import prisma from "../../../lib/prisma";
import { getSession } from "next-auth/react"

export default async (req, res) => {

    let resumes: any;
    let result = { data: { resumes: {} }, message: "" };
    const session = await getSession({ req });

    if (!session || !session.user) {
        result.message = "Unauthorized";
        return res.status(401).json(result);
    }

    if (req.method !== 'GET') {
        result.message = "Method not allowed";
        return res.status(405).json(result);
    }

    try {
        resumes = await prisma.resume.findMany({
            where: {
                userId: session.user.id
            },
            select: {
                id: true,
                title: true,
                createdAt: true,
                archivedAt: true
            }
        });
    } catch (error) {
        result.message = "Something went wrong";
        return res.status(400).json(result);
    }

    result.data = { resumes };
    result.message = "Success";
    return res.status(200).json(result);
}