import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../../lib/prisma";

export default async (request: NextApiRequest, response: NextApiResponse) => {

    let result = { data: {}, message: "" };

    try {

        const { id } = request.body;

        if (!id) {
            result.message = "Missing Id";
            response.status(400).json(result);
            return;
        }

        const resume = await prisma.resume.delete({
            where: { id: id }
        });

        response.status(200).json(resume);

    } catch (error) {

        result.message = "Something went wrong!";
        response.status(400).json(result);
    }
}