import Head from 'next/head'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Welcome to WriteResume</title>
        <meta name="keywords" content="write your resume"></meta>
      </Head>
      <h1>Welcome to WriteResume</h1>
    </div>
  )
}
