import Main from '../components/Main'
import '../styles/globals.css'
import 'antd/dist/antd.css';
import { SessionProvider } from "next-auth/react"

function MyApp({ Component, pageProps }) {
  return (

    <SessionProvider session={pageProps.session}>
      <Main>
        <Component {...pageProps} />
      </Main>
    </SessionProvider>

  )
}

export default MyApp;
