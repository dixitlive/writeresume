import Head from 'next/head'
import { useSession, signIn, signOut } from "next-auth/react"

export default function Login() {

    const { data: session } = useSession()

    return (
        <div>
            <Head>
                <title>Login to your account</title>
                <meta name="keywords" content="write your resume"></meta>
            </Head>
            <h1>Login</h1>
            <h2>Signed in as {JSON.stringify(session)} <br /></h2>
            <button onClick={() => signOut()}>Sign out</button>
            <button onClick={() => signIn()}>Sign in</button>
        </div>
    )
}
