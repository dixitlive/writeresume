import Head from 'next/head'
import { Col, Row, Button, Divider, Space } from 'antd';
import { useRouter } from 'next/router'

/** @param {import('next').InferGetServerSidePropsType<typeof getServerSideProps> } props */
export default function Preview({ resume }) {

    const router = useRouter()
    const { hash } = router.query

    return (
        <>
            <Head>
                <title>Resume {resume.title}</title>
            </Head>
            <Row>
                <Col span={24}>
                    <h1>
                        {resume.title} {hash}
                    </h1>
                </Col>
            </Row>
            <Row>
                <Col span={4}>
                    List of templates (Verticle scroll)
                </Col>
                <Col span={20}>
                    <Row>
                        <Col offset={14}>
                            <Space size={12}>
                                <Button>Download PDF</Button>
                                <Button>Email Resume</Button>
                                <Button>Edit</Button>
                                <Button>Delete</Button>
                            </Space>
                        </Col>
                    </Row>
                    <Divider orientation="left">Preview</Divider>
                    <Row>
                        <Col>
                            Resume
                        </Col>
                    </Row>
                </Col>
            </Row>
        </>
    );
}

export async function getServerSideProps(context) {

    const query = context.query;

    const { NEXT_PUBLIC_URL } = process.env;

    // Fetch data from API
    const response = await fetch(NEXT_PUBLIC_URL + '/api/resumes/view?hash=' + query.hash, {
        headers: {
            cookie: context.req.headers.cookie || "",
        }
    });

    const result = await response.json();
    return { props: { resume: result.data.resume } };
}
