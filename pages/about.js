import Head from 'next/head'

export default function About() {
    return (
        <div>
            <Head>
                <title>About</title>
                <meta name="keywords" content="write your resume"></meta>
            </Head>
            <h1>About</h1>
        </div >
    )
}
