import Head from 'next/head'

export default function Profile() {
    return (
        <div>
            <Head>
                <title>Profile</title>
                <meta name="keywords" content="write your resume"></meta>
            </Head>
            <h1>Profile</h1>
        </div >
    )
}
