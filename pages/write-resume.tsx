import Head from 'next/head'
import {Row, Col, notification, Space} from 'antd';
import { useSession } from 'next-auth/react';
import React, {useEffect, useState} from 'react';
import ContactInfo from '../components/Forms/Resume/ContactInfo';
import CarrierSummary from '../components/Forms/Resume/CarrierSummary';
import SkillInfo from "../components/Forms/Resume/SkillInfo";
import Settings from "../components/Forms/Resume/Settings";

export default function WriteResume(props) {

    console.log('props', props);

    let userId: number;

    //session
    const { data: sessionData } = useSession();

    //setter
    const [resume = {}, setResume] = useState();

    const getResume = () => {
        console.log('Updated Resume', resume);
    }

    //getter
    useEffect(getResume, [resume]);

    if (sessionData && sessionData.user) {
        userId = sessionData.user["id"];
    }

    //setter
    const [isSaving, setIsSaving] = useState(false);

    //ui notifications
    const displayNotification = (message: string, description: any) => {
        notification[message.toLowerCase()]({ message, description });
    };

    //options
    let options = {industries:  props.industries}

    console.log('options', options);

    const saveResume = async (inputFields) => {

        //set saving flag
        setIsSaving(true);

        let formData = {};

        //merge both objects
        formData = {...inputFields, ...formData};

        //add user id
        formData["userId"] = userId;

        //add resume id
        if (resume && resume["id"]) {
            formData["id"] = resume["id"];
        }

        console.log('Save Resume', formData);

        //call save
        const response = await fetch('/api/resumes/store', {
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
            method: "POST",
        });

        //get the results
        const result = await response.json();
        if (response.status == 200 && result.data.resume) {
            setResume(result.data.resume);
        } else {
            displayNotification('Error', result.message)
        }

        //unset saving flag
        setIsSaving(false);
    };

    return (
        <>
            <Head>
                <title>WriteResume</title>
                <meta name="keywords" content="write world class resume"></meta>
            </Head>

            <Row>
                <Col span={12}>
                    <h1>Write Resume - {resume["id"]}</h1>
                </Col>
            </Row>

            <Space
                direction="vertical"
                size="middle"
                style={{display: 'flex'}}>

                {/* Basic Information Block */}
                <Settings resume={resume} saveResume={saveResume} isSaving={isSaving} data={options}></Settings>

                {/* Basic Information Block */}
                <ContactInfo resume={resume} saveResume={saveResume} isSaving={isSaving}></ContactInfo>

                {/* Carrier Summary Block */}
                <CarrierSummary resume={resume} saveResume={saveResume} isSaving={isSaving}></CarrierSummary>

                {/* Skills Block */}
                <SkillInfo resume={resume} saveResume={saveResume} isSaving={isSaving}></SkillInfo>
            </Space>
        </>
    )
}

export async function getServerSideProps(context) {

    const { NEXT_PUBLIC_URL } = process.env;
    let error = "";
    let response = null;
    let industries = null;

    //todo: get resume id from param and get the id

    // Fetch data from API
    try {
        const request = await fetch(NEXT_PUBLIC_URL + '/api/helpers/options', {
            headers: {
                cookie: context.req.headers.cookie || "",
            }
        });

        response = await request.json();
    }catch (e) {
        error = e.toString();
    }

    industries = response.data.industries;
    return { props: { industries } };
}